
Смотрим разницу

```bash
% helmfile --environment production diff
```

Применяем изменения

```bash
% helmfile --environment production apply --concurrency=1
```
